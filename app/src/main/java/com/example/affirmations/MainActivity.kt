package com.example.affirmations

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import com.example.affirmations.adapter.ItemAdapter
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Affirmation

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // TEST THAT WE CAN GET OUR FAKE DATASOURCE:
        //val textView: TextView = findViewById( R.id.textview )
        //textView.text = Datasource().loadAffirmations().size.toString()

        // Use Datasource and Adapter to create a REcyclerView:
        val affirmations = Datasource().loadAffirmations()
        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        recyclerView.adapter = ItemAdapter(this, affirmations)
        recyclerView.setHasFixedSize(true)
    }
}